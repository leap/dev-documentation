# 003: a post-tofu bootstrapping world

* Author: atanarjuat
* Reviewer: your-name-here
* Status: under discussion

## Problem

The old [bonafide
spec](https://0xacab.org/leap/leap_se/-/blob/master/pages/docs/design/bonafide.text)
did place a lot of emphasis on the use of a `TOFU` model to bootstrap trust,
relying on self-signed certificates that were automated by puppet.

Let's Encrypt was announced publicly on November 18, 2014, which is roughly two
years and a half after the beginning of the LEAP project.

I argue that `TOFU` is an unneeded complexity that we keep maintaining because
of incremental changes and cargo-cult mentality. I don't think it adds any
security in a real-world scenario, but it makes boostrapping more complex than
strictly needed.

Additionally, while reviewing this process, I think we can simplify the number
of well-known URLs that are needed to configure the access to the api endpoints
from the TLD of the provider. Some of these files are derived from the fact
that `LEAP` was conceived as a multi-service platform.

## Proposal

* Use Lets Encrypt certificates for all APIs.
* Rely on auto-renewal provided by float, or alternatively configure standalone
  webservers to [provide auto-tls capabilities](https://echo.labstack.com/cookbook/auto-tls/).

## Consequences

## Related proposals

See also `002-vpnweb-deprecation.md` for a proposal about deprecating `vpnweb`
in favor of `menshen` for gateway discovery.
