# LEAP Provider API Endpoints

## Reachable with commercially signed CA certifcates:
**Provider JSON**, contains base information for bootstrapping the client. It is recommended to preship the provider.json in the client, contains a fingerprint for the ca.crt: `subdomain.example.org/provider.json`

_Example URL_: `black.riseup.net/provider.json`

_Example Content_: [provider.json](api_specs/v3/provider.json)

Two ways to proceed further either via option (a) Let's Encrypt singned endpoint or option (b) self signed CA endpoint.

**Self signed CA certificate** can be obtained from: `subdomain.example.org/ca.crt`

_Example URL_: `black.riseup.net/ca.crt`

_Example content_: [ca.crt](api_specs/v3/ca.crt)

## (a) Reachable with the Let's Encrypt signed CA certificates:

**Provider JSON**, if the client has a valid CA certificate it's recommended to use this endpoint to update the Provider JSON: `api.subdomain.example.org/provider.json`

_Exmaple URL_: `https://api.black.riseup.net/provider.json`

_Example Content_: [provider.json](api_specs/v3/provider.json)

**Encrypted Internet Service JSON**, contains information about connecting to encrypted internet service gateways and their capabilities: `api.subdomain.exmaple.org/<APIVERSION>/config/eip-service.json`

_Example URL_: `https://api.black.riseup.net/3/config/eip-service.json`

_Example Content_: [eip-service.json](api_specs/v3/eip-service.json)

**Openvpn client cert**, client cert can be obtained from : `https://api.subdomain.example.org/3/cert`

_Exmaple URL_: `https://api.black.riseup.net/3/cert`

_Example Content_: [cert.pem](api_specs/v3/cert.pem)

## (b) Reachable with the self signed CA certificates:

**Provider JSON**, if the client has a valid CA certificate it's recommended to use this endpoint to update the Provider JSON: `api.subdomain.example.org:<PORT>/provider.json`

_Exmaple URL_: `https://api.black.riseup.net:4430/provider.json`

_Example Content_: [provider.json](api_specs/v3/provider.json)

**Encrypted Internet Service JSON**, contains information about connecting to encrypted internet service gateways and their capabilities: `api.subdomain.exmaple.org:<PORT>/<APIVERSION>/config/eip-service.json`

_Example URL_: `https://api.black.riseup.net:443/3/config/eip-service.json`

_Example Content_: [eip-service.json](api_specs/v3/eip-service.json)

**Openvpn client cert**, client cert can be obtained from : `api.subdomain.example.org:<PORT>/3/cert`

_Exmaple URL_: `https://api.black.riseup.net:4430/3/cert`

_Example Content_: [cert.pem](api_specs/v3/cert.pem)


**Menshen Service geoservice endpoint[deprecated]**, serves an sorted list the client should try incremantally to connect to a gateway. The sorting depends on the users network location and the load of each gateway (tbd.): `api.subdomain.example.org:<PORT>/json`

_Example URL_: `https://api.black.riseup.net:9001/json`

_Example Content_: [geoservice.json](./v3/geoservice.json)